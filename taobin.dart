import 'dart:io';

class taobin {
  var menu1;
  var sweet;
  int price = 0;
  menu() {
    print('เลือกเมนู');
    print('1.เมนูแนะนำ');
    print('2.กาแฟ');
    print('3.ชา');
    print('4.นม โกโก้และคาราเมล');
    print('5.โปรตีนเชค');
    print('6.น้ำโซดา');

    int Category = int.parse(stdin.readLineSync()!);
    if (Category == 1) {
      Recommended();
    }
    if (Category == 2) {
      Coffee();
    }
    if (Category == 3) {
      Tea();
    }
    if (Category == 4) {
      milk();
    }
    if (Category == 5) {
      proteinshake();
    }
    if (Category == 6) {
      soda();
    }
  }

  Sweet() {
    print('หวานน้อย');
    print('หวานปกติ');
    print('หวานมาก');
    sweet = int.parse(stdin.readLineSync()!);
    if (sweet == 1) {
      sweet = 'หวานน้อย';
    }
    if (sweet == 2) {
      sweet = 'หวานปกติ';
    }
    if (sweet == 3) {
      sweet = 'หวานมาก';
    }
  }

  Recommended() {
    print('นมสตอเบอร์รี่ปั่น 45 บาท');
    print('โกโก้สตอเบอร์รี่ปั่น 45 บาท');
    print('โอริโอ้ปั่นภูเขาไฟ 55 บาท');
    menu1 = int.parse(stdin.readLineSync()!);
    if (menu1 == 1) {
      menu1 = 'นมสตอเบอร์รี่ปั่น 45 บาท';
      price += 45;
    }
    if (menu1 == 2) {
      menu1 = 'โกโก้สตอเบอร์รี่ปั่น 45 บาท';
      price += 45;
    }
    if (menu1 == 3) {
      menu1 = 'โอริโอ้ปั่นภูเขาไฟ 55 บาท';
      price += 55;
    }
    Sweet();
  }

  Coffee() {
    print('อเมริกาโน่ เย็น 35 บาท');
    print('ลาเต้ เย็น 40 บาท');
    print('คาปูชิโน่ เย็น 45 บาท');
    menu1 = int.parse(stdin.readLineSync()!);
    if (menu1 == 1) {
      menu1 = 'อเมริกาโน่ เย็น 35 บาท';
      price += 35;
    }
    if (menu1 == 2) {
      menu1 = 'ลาเต้ เย็น 40 บาท';
      price += 40;
    }
    if (menu1 == 3) {
      menu1 = 'คาปูชิโน่ เย็น 45 บาท';
      price += 45;
    }
    Sweet();
  }

  Tea() {
    print('ชาไทย เย็น 40 บาท');
    print('ชาขิง ร้อน 20 บาท');
    print('ชาไต้หวันร้อน 35 บาท');
    if (menu1 == 1) {
      menu1 = 'ชาไทย เย็น 40 บาท';
      price += 40;
    }
    if (menu1 == 2) {
      menu1 = 'ชาขิง ร้อน 20 บาท';
      price += 20;
    }
    if (menu1 == 3) {
      menu1 = 'ชาไต้หวันร้อน 35 บาท';
      price += 35;
    }
    Sweet();
  }

  milk() {
    print('นมร้อน 30 บาท');
    print('นมคาราเมล 30 บาท');
    print('โกโก้ เย็น 30 บาท');
    menu1 = int.parse(stdin.readLineSync()!);
    if (menu1 == 1) {
      menu1 = 'นมร้อน 30 บาท';
      price += 30;
    }
    if (menu1 == 2) {
      menu1 = 'นมคาราเมล 30 บาท';
      price += 30;
    }
    if (menu1 == 3) {
      menu1 = 'โกโก้ เย็น 30 บาท';
      price += 30;
    }
    Sweet();
  }

  proteinshake() {
    print('มัทฉะโปรตีน 60 บาท');
    print('โกโก้โปรตีน 60 บาท');
    print('โปรตีนเปล่า 60 บาท');
    menu1 = int.parse(stdin.readLineSync()!);
    if (menu1 == 1) {
      menu1 = 'มัทฉะโปรตีน 60 บาท';
      price += 60;
    }
    if (menu1 == 2) {
      menu1 = 'โกโก้โปรตีน 60 บาท';
      price += 60;
    }
    if (menu1 == 3) {
      menu1 = 'โปรตีนเปล่า 60 บาท';
      price += 60;
    }
    Sweet();
  }

  soda() {
    print('น้ำแดงสละโซดา 20 บาท');
    print('น้ำมะนาวโซดา 20 บาท');
    print('น้ำบ๊วยโซดา 20 บาท');
    menu1 = int.parse(stdin.readLineSync()!);
    if (menu1 == 1) {
      menu1 = 'น้ำแดงสละโซดา 20 บาท';
      price += 20;
    }
    if (menu1 == 2) {
      menu1 = 'น้ำมะนาวโซดา 20 บาท';
      price += 20;
    }
    if (menu1 == 3) {
      menu1 = 'น้ำบ๊วยโซดา 20 บาท';
      price += 20;
    }
    Sweet();
  }

  printString() {
    print('เมนูที่เลือกคือ ' + menu1);
    print('ราคารวม ' + price.toString() + ' ' + sweet.toString());
    print('ขอบคุณที่ใช้บริการ');
  }
}

void main(List<String> args) {
  taobin tb = new taobin();
  tb.menu();
  tb.printString();
}
